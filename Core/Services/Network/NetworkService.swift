//
//  NetworkService.swift
//  Core
//
//  Created by Frezy Mboumba on 12/20/18.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation
import RxSwift

public enum NetworkURL: CaseIterable {}

public protocol NetworkServiceDefault: class {
    func execute<T>() -> Observable<T> where T: Decodable, T: Encodable
}

public class NetworkService: NetworkServiceDefault {
    public func execute<T>() -> Observable<T> {
        return Observable.empty()
    }
}
