//
//  AppCoordinator.swift
//  Expora iOS
//
//  Created by Frezy Mboumba on 12/20/18.
//  Copyright © 2018 Expora. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    // MARK: - PUBLIC ATTRIBUTES

    var childCoordinators: [Coordinator] = []

    var rootViewController: UIViewController {
        return navigationController
    }

    // MARK: - PRIVATE ATTRIBUTES

    fileprivate let window: UIWindow

    fileprivate let navigationController: UINavigationController = {
        let navController = UINavigationController()
        navController.isNavigationBarHidden = true
        return navController
    }()

    // MARK: - INITIALIZERS

    init(window: UIWindow) {
        self.window = window
        self.window.rootViewController = self.rootViewController
        self.window.makeKeyAndVisible()
    }

    // MARK: - PUBLIC METHODS

    func start() {
        self.showARCoordinator()
    }

    fileprivate func showARCoordinator() {
        let coordinator = ARCoordinator()
        coordinator.start()
        self.addChildCoordinator(coordinator)
        self.window.rootViewController = coordinator.rootViewController
    }

}
