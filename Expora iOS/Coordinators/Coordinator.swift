//
//  Coordinator.swift
//  Expora iOS
//
//  Created by Frezy Mboumba on 12/20/18.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation
import UIKit

public protocol BaseCoordinator: class {

    var childCoordinators: [Coordinator] { get set }
    func start()
}

public protocol RootCoordinator: class {
    var rootViewController: UIViewController { get }
}

public extension BaseCoordinator {

    /// Add a child coordinator to the parent
    public func addChildCoordinator(_ childCoordinator: Coordinator) {
        self.childCoordinators.append(childCoordinator)
    }

    /// Remove a child coordinator from the parent
    public func removeChildCoordinator(_ childCoordinator: Coordinator) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }

}

public typealias Coordinator = BaseCoordinator & RootCoordinator
