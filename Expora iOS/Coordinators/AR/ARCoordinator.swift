//
//  ARCoordinator.swift
//  Expora iOS
//
//  Created by Frezy Mboumba on 12/20/18.
//  Copyright © 2018 Expora. All rights reserved.
//

import UIKit
import RxSwift

class ARCoordinator: Coordinator {

    // MARK: - PUBLIC ATTRIBUTES

    var childCoordinators: [Coordinator] = []

    var rootViewController: UIViewController {
        return navigationController
    }

	fileprivate let bag: DisposeBag = DisposeBag()
	fileprivate var ARVC :ARViewController!

    // MARK: - PRIVATE ATTRIBUTES

    fileprivate let navigationController: UINavigationController = {
        let navController = UINavigationController()
        navController.isNavigationBarHidden = true
        return navController
    }()

    // MARK: - PUBLIC METHODS

    func start() {
        self.showArKitVC()
    }

    // MARK: - HELPER METHODS

    fileprivate func showArKitVC() {
        self.ARVC = ARViewController()
		self.ARVC.shouldPresentSeriesVC.observeOn(MainScheduler.instance).subscribe {
			self.showSeriesVC()
			}.disposed(by: bag)
        self.navigationController.viewControllers = [self.ARVC]
    }

	fileprivate func showSeriesVC() {
		let vc = SeriesViewController()
		vc.shouldDismissSeriesVC.observeOn(MainScheduler.instance).subscribe(onNext: { videoTitle in
			self.dismissSeriesVC()
			self.ARVC.showVideo(videoTitle: videoTitle)
		}).disposed(by: bag)
		vc.modalPresentationStyle = .overCurrentContext
		vc.modalTransitionStyle = .crossDissolve
		self.rootViewController.present(vc, animated: true, completion: nil)
	}

	fileprivate func dismissSeriesVC () {
		let vc = ARViewController()
		vc.shouldShowTrailer.onCompleted()
		self.rootViewController.dismiss(animated: true, completion: nil)
	}

}
