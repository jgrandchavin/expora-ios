//
//  AppDelegate.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 24/10/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		setupApp()
        setupFabrics()
        return true
    }

    // MARK: - HELPER METHODS

	fileprivate func setupApp() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if let window = self.window {
            let appCoordinator = AppCoordinator(window: window)
            appCoordinator.start()
        }
	}

    @discardableResult
    fileprivate func setupFabrics() -> Fabric {
        return Fabric.with([Crashlytics.self])
    }

}
