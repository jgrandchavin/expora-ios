//
//  RessourcesEnum.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 12/12/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation

enum ARResourcesScenes: String {
	case PikachuScenePath = "Scenes.scnassets/pikachu.scn"
	case PikachuSceneName = "Pikachu"
}
