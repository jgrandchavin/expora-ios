//
//  ARRessourcesImages.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 12/12/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation

enum ARResourcesImages: String {
	case GroupName = "AR Resources"
	case ThunderstormImageName = "thunderstorm"
}
