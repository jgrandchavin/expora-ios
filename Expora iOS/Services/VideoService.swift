//
//  VideoService.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 21/01/2019.
//  Copyright © 2019 Expora. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

class VideoService {

	fileprivate var videoNode: SKVideoNode!

	public func showVideo(name: String, sceneView: ARSCNView) {

		guard let currentFrame = sceneView.session.currentFrame else {
			return
		}

		let videoNode = SKVideoNode(fileNamed: name)
		videoNode.play()

		let skScene = SKScene(size: CGSize(width: 640, height: 480))
		skScene.addChild(videoNode)

		videoNode.position = CGPoint(x: skScene.size.width/2, y: skScene.size.height/2)
		videoNode.size = skScene.size

		let tvPlane = SCNPlane(width: 1.0, height: 0.75)
		tvPlane.firstMaterial?.diffuse.contents = skScene
		tvPlane.firstMaterial?.isDoubleSided = true

		let tvPlaneNode = SCNNode(geometry: tvPlane)

		var translation = matrix_identity_float4x4
		translation.columns.3.z = -1.0

		tvPlaneNode.simdTransform = matrix_multiply(currentFrame.camera.transform, translation)
		tvPlaneNode.eulerAngles = SCNVector3(Double.pi, 0, 0)

		sceneView.scene.rootNode.addChildNode(tvPlaneNode)
		sceneView.isPlaying = true
	}
}
