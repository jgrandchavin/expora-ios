//
//  AudioService.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 12/12/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation
import AVFoundation

class AudioService {

	fileprivate var audioPlayer: AVAudioPlayer?

	/**
	Play a ambiance sound in function of the image which is detected.

	- Parameter soundName: The image's name wich is detected.
	*/
	public func playAmbianceSound(soundName: String) {
		do {
			guard let fileURL = Bundle.main.url(forResource: soundName, withExtension: "mp3") else {
				fatalError("Failed to load sound")
			}
			audioPlayer = try AVAudioPlayer(contentsOf: fileURL)
			audioPlayer?.play()
		} catch {
			fatalError("Failed to play sound")
		}
	}

}
