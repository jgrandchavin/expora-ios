//
//  ARViewController.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 24/10/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import UIKit
import ARKit
import SceneKit
import RxSwift

class ARViewController: UIViewController {

    // MARK: - @IBOUTLETS

	fileprivate var sceneView: ARSCNView!
	fileprivate let bag: DisposeBag = DisposeBag()
	fileprivate let videoService = VideoService()

    // MARK: - PUBLIC ATTRIBUTES
	let shouldPresentSeriesVC = PublishSubject<Void>()
	let shouldShowTrailer = PublishSubject<Void>()

    // MARK: - LIFECYCLE METHODS

	override func viewDidLoad() {
		super.viewDidLoad()
		setUpSceneView()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		sceneView.session.pause()
	}

	// MARK: - PUBLIC METHODS

	public func showVideo(videoTitle :String) {
		self.videoService.showVideo(name: videoTitle, sceneView: self.sceneView)
	}

	// MARK: - PRIVATE METHODS

	private func setUpSceneView() {
		sceneView = ARSCNView(frame: self.view.bounds)
		self.view.addSubview(sceneView)

		guard let referenceImage = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
			return
		}

		let configuration = ARWorldTrackingConfiguration()
		configuration.detectionImages = referenceImage
		configuration.planeDetection = [.horizontal, .vertical]

		sceneView.delegate = self
		sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
	}
}

extension ARViewController: ARSCNViewDelegate {
	func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
		DispatchQueue.main.async {
			guard let imageAnchor = anchor as? ARImageAnchor else { return }

			let referenceImage = imageAnchor.referenceImage
			let referenceImageName = referenceImage.name ?? "no name"

			self.shouldShowTrailer.observeOn(MainScheduler.instance).subscribe {
				print("salut")
				}.disposed(by: self.bag)

			switch referenceImageName {
			case "netflix" :
				//self.videoService.showVideo(name: "got-trailer.mp4", sceneView: self.sceneView)
				self.shouldPresentSeriesVC.onCompleted()
			default:
				break
			}
		}
	}
}
