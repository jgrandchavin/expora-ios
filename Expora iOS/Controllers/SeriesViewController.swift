//
//  SeriesViewController.swift
//  Expora iOS
//
//  Created by Julien Grand-Chavin on 20/12/2018.
//  Copyright © 2018 Expora. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SeriesViewController: UIViewController {

	// MARK: - @IBOUTLETS

	fileprivate var titleLabel: UILabel!
	fileprivate var imageView: UIImageView!
	fileprivate var image: UIImage!
	fileprivate var imageLabel: UILabel!
	fileprivate var serieNameView: UIView!
	fileprivate var watchInARButton: UIButton!

	// MARK: - PUBLIC ATTRIBUTES

	let shouldDismissSeriesVC = PublishSubject<String>()

	// MARK: - LIFECYCLE METHODS

	override func viewDidLoad() {
		super.viewDidLoad()
		setUpView()
		setUpTitle()
		setUpImage()
		setUpImageLabel()
		setUpWatchInARButton()
	}

	private func setUpView() {
		let blurEffect = UIBlurEffect(style: .light)
		let blurEffectView = UIVisualEffectView(effect: blurEffect)
		blurEffectView.frame = view.frame
		view.addSubview(blurEffectView)
	}

	private func setUpTitle() {
		titleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: view.frame.width, height: 100))
		view.addSubview(titleLabel)
		//titleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20.0).isActive = true
		//titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
		titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
		titleLabel.text = "SÉRIE DU MOMENT"
		titleLabel.textAlignment = .center
		titleLabel.textColor = .white
		titleLabel.font = titleLabel.font.withSize(30.0)
	}

	private func setUpImage() {
		let imageName = "got"
		image = UIImage(named: imageName)
		imageView = UIImageView(image: image)
		view.addSubview(imageView)
		imageView.translatesAutoresizingMaskIntoConstraints = false
		imageView.layer.cornerRadius = 15
		imageView.clipsToBounds = true
		imageView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 10).isActive = true
		imageView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 40).isActive = true
		imageView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -40).isActive = true
		imageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -250).isActive = true

		imageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
	}

	private func setUpImageLabel() {
		serieNameView = UIView()
		serieNameView.backgroundColor = UIColor(red: 30/255, green: 39/255, blue: 46/255, alpha: 1)

		self.view.addSubview(serieNameView)

		serieNameView.translatesAutoresizingMaskIntoConstraints = false
		serieNameView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 30).isActive = true
		serieNameView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
		serieNameView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
		serieNameView.heightAnchor.constraint(equalToConstant: 50).isActive = true
		serieNameView.layer.cornerRadius = 10

		imageLabel = UILabel()

		serieNameView.addSubview(imageLabel)

		imageLabel.translatesAutoresizingMaskIntoConstraints = false
		imageLabel.text = "Game of Thrones"
		imageLabel.centerXAnchor.constraint(equalTo: serieNameView.centerXAnchor).isActive = true
		imageLabel.centerYAnchor.constraint(equalTo: serieNameView.centerYAnchor).isActive = true
		imageLabel.textColor = UIColor.white
	}

	private func setUpWatchInARButton() {
		watchInARButton = UIButton()
		watchInARButton.backgroundColor = UIColor.white
		watchInARButton.setTitle("Regarder la bande annonce en AR", for: UIControl.State.normal)
		watchInARButton.setTitleColor(UIColor.black, for: UIControl.State.normal)

		self.view.addSubview(watchInARButton)

		watchInARButton.translatesAutoresizingMaskIntoConstraints = false
		watchInARButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
		watchInARButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
		watchInARButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
		watchInARButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
		watchInARButton.layer.cornerRadius = 22.5

		watchInARButton.addTarget(self, action: #selector(self.pressButton(_:)), for: .touchUpInside)
	}

	@objc func pressButton(_ sender: UIButton) {
		self.shouldDismissSeriesVC.onNext("got-trailer.mp4")
	}
}
